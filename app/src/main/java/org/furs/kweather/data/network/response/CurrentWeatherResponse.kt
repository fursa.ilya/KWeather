package org.furs.kweather.data.network.response

import com.google.gson.annotations.SerializedName
import org.furs.kweather.data.network.response.model.CurrentWeather
import org.furs.kweather.data.network.response.model.Location
import org.furs.kweather.data.network.response.model.Request


data class CurrentWeatherResponse(
    @SerializedName("current")
    val currentWeather: CurrentWeather,
    val location: Location,
    val request: Request
)